import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import About from './components/about/About';
import { Classroom } from './components/classroom/Classroom';
import Header from './components/layout/Header';

function App() {
  return (
    <BrowserRouter>
      <Header></Header>
      <Switch>
        <Route exact path="/" >
          <Classroom nom="JS Dev" />
        </Route>
        <Route path="/about" component={About} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
