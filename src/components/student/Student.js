import React from 'react'

function Student({learner, handleDelete}) {
    return (
        <div className = "centrer" style={{ textAlign:"center", borderBottom:"1px dotted", width:"200px"}}>
            { learner.nom }
            <button style={ {float:"right"}} onClick={handleDelete.bind(this, learner.id)}>X</button>
        </div>
    )
}

export default Student
