import axios from 'axios';
import React, { Component } from 'react'
import AddStudent from '../addStudent/AddStudent';
import Student from '../student/Student'

export class Classroom extends Component {

    constructor() {
        super();
        this.state = {
            isLoading: true, students: [], error:null
        }

        this.handleDelete = this.handleDelete.bind(this);
    }

    componentDidMount() {
        axios.get('http://localhost:3333/learners')
            .then(res => {
                const students = res.data;
                this.setState({ students, isLoading:false });
            })
            .catch(error => this.setState({ error, isLoading: false }))
    }

    render() {
        const learners = this.state.students;
        const isLoading = this.state.isLoading;
        if(!learners.length) {
            return (
                <div className = "centrer">
                    <AddStudent handleAdd = {this.handleAdd} />
                    <h1>Liste des étudiants de { this.props.nom }</h1>
                    <p>Aucun étudiant dans cette classe !</p>
                </div>
            )
        }
        return (
            <div className = "centrer">
                <AddStudent handleAdd = {this.handleAdd} />
                <h1>Liste des étudiants de { this.props.nom }</h1>
                {
                    (isLoading) ? <p>Loading ...</p>:
                    learners.map(learner => <Student
                        key={learner.id}
                        learner={learner}
                        handleDelete = {this.handleDelete}
                    />)
                }
            </div>
        )
    }

    handleDelete(id) {
        axios.delete('http://localhost:3333/learners/'+id)
        .then(res => this.setState( prevState => (
            {students : prevState.students.filter(
                student => student.id !== id
            )}
        )))
        .catch(error => this.setState( {error:error, isLoading: false}) )
    }

    handleAdd = nom => {
        axios.post('http://localhost:3333/learners/', {nom})
        .then(res => {
            this.setState({ students : [...this.state.students, res.data] })
        })     
    }
}

export default Student