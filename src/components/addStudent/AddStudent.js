import React, { Component } from 'react'

export class AddStudent extends Component {
    constructor() {
        super();
        this.state = { nom:'', placeholder:"Nom de l'étudiant" }
    }
    handleChange = event => this.setState({nom:event.target.value});
    render() {
        return (
            <div style={{marginTop:"5px"}} >
                <form onSubmit={this.handleAdd}>
                    <input
                        name = "nom"
                        placeholder = {this.state.placeholder}
                        value = {this.state.value}
                        onChange = {this.handleChange}
                    />
                    <input type="submit" value="Ajouter"/>
                </form>
            </div>
        )
    }

    handleAdd = e => {
        e.preventDefault();
        this.props.handleAdd(this.state.nom);
        this.setState({nom:''});
    }
}

export default AddStudent

