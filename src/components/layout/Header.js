import { Link } from "react-router-dom"

function Header() {
    return (
        <header className = "centrer">
            <h1>React Classroom App</h1>
            <nav>
                <Link to="/">Home</Link>&nbsp;|
                <Link to="/about">About</Link>
            </nav>
        </header>
    )
}

export default Header;