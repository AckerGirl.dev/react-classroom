import React from 'react'

function About () {
    return (
        <React.Fragment>
            <h1>A propos de <strong><em>React Classroom App Version 1.0.0</em></strong></h1>
            <p>Gestion d'étudiants : lister, ajouter et supprimer</p>
        </React.Fragment>
    )
}

export default About
